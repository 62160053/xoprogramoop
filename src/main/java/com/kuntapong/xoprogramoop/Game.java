/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuntapong.xoprogramoop;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class Game {

    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int row, col;
    int round = 0;
    Scanner kb = new Scanner(System.in);

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void showWelcome() {
        System.out.println("Welcome to XO Game");
    }

    public void showTable() {
        table.showTable();
    }

    public void input() {
        while (true) {
            System.out.println("Please input Row Col: ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            //      System.out.println("row: "+ row + "col: "+ col);
            if (table.setRowCol(row, col)) {
                break;
            }
            System.out.println("Error: table at row and col not empty!!");
        }
    }

    public void showTurn() {
        System.out.println(table.getCurrentPlayer().getName() + " turn");
    }
    public void newGame(){
        table = new Table(playerX, playerO);
    }
    public void run() {
        this.showWelcome();
        while (true) {
            this.showTable();
            this.showTurn();
            this.input();
            table.CheckWin();
            round++;
            if (table.isFinish()) {
                if (table.getWin() == null) {
                    System.out.println("Draw!!");
                } else {
                    System.out.println(table.getWin().getName() + " win");

                }
                System.out.println("Pass Y if Do you want to start a new game");
                char x = kb.next().charAt(0);
                if(x == 'y'|| x == 'Y'){
                    newGame();
                }else{
                    break;
                }
            }
            table.switchPlayer();
        }
    }
}
